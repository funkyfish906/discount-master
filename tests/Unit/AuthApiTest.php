<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSignIn()
    {
        $this->json('GET', 'api/customer/signin/sarah/1235')
            ->assertJson([
                'error_code' => 200,
            ]);
    }
}
