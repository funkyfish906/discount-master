<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;




class Companies extends Model {

    

    

    protected $table    = 'companies';
    
    protected $fillable = ['name'];
    

    public static function boot()
    {
        parent::boot();

        Companies::observe(new UserActionsObserver);
    }
    
    
    
    
}