<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


//use Illuminate\Database\Eloquent\SoftDeletes;

class UserCompanies extends Model {

    //use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $table    = 'user_companies';

    protected $fillable = [
        'user_id',
        'company_id',
        'bonus_account'
    ];


    public static function boot()
    {
        parent::boot();

        TradePoints::observe(new UserActionsObserver);
    }


    public function company()
    {
        return $this->belongsTo(Companies::class, 'company_id');
    }


}