<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


//use Illuminate\Database\Eloquent\SoftDeletes;

class TradePoints extends Model {

    //use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    //protected $dates = ['deleted_at'];
    
    protected $primaryKey = 'id';

    protected $table    = 'tradepoints';
    
    protected $fillable = [
          'name',
          'logo',
          'connected',
          'bonus',
          'description',
          'country',
          'city',
          'address',
          'footnote',
          'phone',
          'email',
          'website',
          'schedule'
    ];
    

    public static function boot()
    {
        parent::boot();

        TradePoints::observe(new UserActionsObserver);
    }


    public function customer()
    {
        return $this->hasOne(Customers::class, 'id');
    }

    public function company()
    {
        return $this->belongsTo(Companies::class, 'company_id', 'id');
    }

    public function balance()
    {
        return $this->hasMany(UserCompanies::class, 'id', 'company_id');
    }

    public function friends()
    {
        return $this->hasMany(UserCompanies::class, 'id', 'company_id');
    }

    
}