<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionList extends Model
{
    protected $primaryKay    = 'id';
    
    protected $fillable = [
    	'user_id',
        'transaction',
        'operation'
    ];


    public function user()
    {
        return $this->belongsTo(AppUser::class, 'user_id');
    }
}
