<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
    protected $primaryKay    = 'id';

    protected $fillable = [
        'phone',
        'code'
    ];
}
