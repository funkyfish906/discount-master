<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;


use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model implements AuthenticatableContract{

    use SoftDeletes;
    use Authenticatable;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $table    = 'customers';
    
    protected $fillable = [
          'username',
          'password',
          'tradepoints_id',
    ];
    

    public static function boot()
    {
        parent::boot();

        Customers::observe(new UserActionsObserver);
    }
    
    public function tradepoint()
    {
        return $this->belongsTo(TradePoints::class, 'tradepoints_id');
    }
    
    
}