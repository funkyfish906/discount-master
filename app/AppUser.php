<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;


class AppUser extends Model implements AuthenticatableContract
{
    use Authenticatable;
    protected $primaryKay    = 'id';

    protected $fillable = [
        'phone',
        'password'
    ];
}
