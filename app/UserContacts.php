<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


//use Illuminate\Database\Eloquent\SoftDeletes;

class UserContacts extends Model {

    //use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $table    = 'user_contacts';

    protected $fillable = [
        'user_id',
        'phone',
        'name',
        'email',
        'photo'
    ];


    public static function boot()
    {
        parent::boot();

        TradePoints::observe(new UserActionsObserver);
    }



}