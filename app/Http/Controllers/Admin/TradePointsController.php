<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use Exception;
use App\Http\Requests\CreateTradePointsRequest;
use App\Http\Requests\UpdateTradePointsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\TradePoints;
use App\PhotoGallery;
use App\Schedule;


class TradePointsController extends Controller {

    public static $weekdays = array('days' => ['Понедельник','Вторник','Середа','Четверг','Пятница','Суббота','Воскресение']);

	/**
	 * Display a listing of tradepoints
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $tradepoints = TradePoints::all();

		return view('admin.tradepoints.index', compact('tradepoints'));
	}

	/**
	 * Show the form for creating a new tradepoints
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    return view('admin.tradepoints.create')->with(['weekdays' => self::$weekdays]);
	}

	/**
	 * Store a newly created tradepoints in storage.
	 *
     * @param CreateTradePointsRequest|Request $request
	 */
	public function store(CreateTradePointsRequest $request)
	{
		$data = $request->all();
		
	    $request = $this->saveFiles($request);
	    $tradePoint = new TradePoints;
	    $tradePoint->name = $data['name'];
	    $tradePoint->connected = $data['connected'];	   
	    $tradePoint->bonus = $data['bonus'];	   
	    $tradePoint->description = $data['description'];
	    $tradePoint->country = $data['country'];
	    $tradePoint->city = $data['city'];
	    $tradePoint->address = $data['address'];
	    $tradePoint->footnote = $data['footnote'];
	    $tradePoint->phone = $data['phone'];
	    $tradePoint->email = $data['email'];
	    $tradePoint->website = $data['website'];

        $schedule = array();
        $tmp = 0;
        foreach ($data['time'] as $time){
            if ($time != null) $tmp = 1;
        }
        if ($tmp){
            $schedule['time'] = $data['time'];
            $schedule['break'] = $data['break'];
            $tradePoint->schedule = json_encode($schedule,JSON_FORCE_OBJECT);
        }

        if ($request->file('logo') != null) {
            dd($_FILES);

            $imageName = str_random(30) . '.' .
                $request->file('logo')->getClientOriginalExtension();

            $request->file('logo')->move(
                base_path() . '/public/images/trades/', $imageName
            );

            $path = '/images/trades/' . $imageName;
            $tradePoint->logo = $path;
        }


        $tradePoint->save();

		return redirect()->route(config('quickadmin.route').'.tradepoints.index');
	}

	/**
	 * Show the form for editing the specified tradepoints.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tradepoints = TradePoints::find($id);
        $schedule = json_decode($tradepoints->schedule, true);
        $schedule = array();
        try {
        	$schedule = array_merge($schedule, self::$weekdays);
        }
        catch(Exception $exception){
        	$schedule = array();
        }

		return view('admin.tradepoints.edit')->with(['tradepoints' => $tradepoints, 'schedule' => $schedule]);
	}

	/**
	 * Update the specified tradepoints in storage.
     * @param UpdateTradePointsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTradePointsRequest $request)
	{
		$data = $request->all();

		$tradepoints = TradePoints::findOrFail($id);
		$tradepoints->update($data);

        if ($request->file('logo') != null) {


            $imageName = str_random(30) . '.' .
                $request->file('logo')->getClientOriginalExtension();

            $request->file('logo')->move(
                base_path() . '/public/images/trades/', $imageName
            );

            $path = '/images/trades/' . $imageName;
            $tradepoints->logo = $path;
        }

        $schedule = array();
        $tmp = 0;
        foreach ($data['time'] as $time){
            if ($time != null) $tmp = 1;
        }
        if ($tmp){
            $schedule['time'] = $data['time'];
            $schedule['break'] = $data['break'];
            $tradepoints->schedule = json_encode($schedule,JSON_FORCE_OBJECT);
        }
		$tradepoints->save();

		return redirect()->route(config('quickadmin.route').'.tradepoints.index');
	}

	/**
	 * Remove the specified tradepoints from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		TradePoints::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tradepoints.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            TradePoints::destroy($toDelete);
        } else {
            TradePoints::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tradepoints.index');
    }


    public function show($id)
	{
		$tradepoints = TradePoints::find($id);
		$gallery = PhotoGallery::where('company_id',$id)->get();
        $schedule = json_decode($tradepoints->schedule, true); 

        try {
            if (!empty($schedule))
        	$schedule = array_merge($schedule, self::$weekdays);
        }
        catch(Exception $exception){
        	$schedule = array();
        }
       // dd($schedule);

		return view('admin.tradepoints.show')->with(['tradepoints' => $tradepoints, 'gallery' => $gallery, 'schedule' => $schedule]);
	}

}
