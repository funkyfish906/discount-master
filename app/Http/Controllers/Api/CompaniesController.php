<?php

namespace App\Http\Controllers\Api;

use App\AppUser;
use App\TradePoints;
use App\Companies;
use App\UserCompanies;
use App\UserContacts;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoapClient;
use Swagger\Annotations as SWG;
use League\Geotools\Coordinate\Ellipsoid;
use Toin0u\Geotools\Facade\Geotools;

class CompaniesController extends Controller
{
    public function sort(){
            $args = func_get_args();
            $data = array_shift( $args );
            if ( ! is_array( $data ) ) {
                return array();
            }
            $multisort_params = array();
            foreach ( $args as $n => $field ) {
                if ( is_string( $field ) ) {
                    $tmp = array();
                    foreach ( $data as $row ) {
                        $tmp[] = $row[ $field ];
                    }
                    $args[ $n ] = $tmp;
                }
                $multisort_params[] = &$args[ $n ];
            }
            $multisort_params[] = &$data;
            call_user_func_array( 'array_multisort', $multisort_params );
            return end( $multisort_params );
    }

    public function distance(Request $request)
    {
        $coordA = Geotools::coordinate([48.8234222, 2.3072]);
        $coordB = Geotools::coordinate([43.296482, 5.36978]);
        $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);
        return response()->json(['distance' => $distance->flat()]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function usercontacts(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (!$user) return response()->json(['error_code' => 401]);

        $contacts = json_decode($request->contacts, true);
        //dd($contacts['contacts_from_user']);

        if (!$contacts) return response()->json(['error_code' => 403]);
        $friends = array();
        foreach ($contacts['contacts_from_user'] as $item){

            $contact = new UserContacts;
            $contact->user_id = $user->id;
            $contact->phone = $item['phone'];
            $contact->name = $item['name'];
            $contact->email = $item['email'];
            $contact->save();
            $appuser = AppUser::where('phone', $item['phone'])
                              ->orWhere('phone', 'like', '%' .  $item['phone'] . '%')->first();
            if (count($appuser)){
                $friends[] = array(
                                'user_id' => $user->id,
                                'friend_id' => $appuser->id,
                                'contact_id' => $contact->id,
                                );
            }
        }
        DB::table('user_friends')->insert($friends);
        //UserContacts::create(json_decode($request->contacts,true));

        return response()->json(['error_code' => 200, 'friends' => $friends]);

    }

    public function companyinfo(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (!$user) return response()->json(['error_code' => 401]);

        $company = Companies::find($request->id);

        $points = TradePoints::where('company_id', $request->id)->get();

        if (!$company) return response()->json(['error_code' => 404]);

        return response()->json(['error_code' => 200, 'company' => $company, 'points' => $points]);

    }

    public function tradepointinfo(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (!$user) return response()->json(['error_code' => 401]);

        $tradepoint = TradePoints::find($request->id);

        if (!$tradepoint) return response()->json(['error_code' => 404]);

        return response()->json(['error_code' => 200, 'point' => $tradepoint]);

    }

    public static function getcompanies(AppUser $user)
    {
        $userCoord = Geotools::coordinate([$user->lat, $user->lng]);


        $friends_row = array();
        $distance_row = array();

        $allpoints = array();
        $mypoints = array();
        $friendspoints = array();
        $tradepoints = TradePoints::all();
        $my_accounts = UserCompanies::where('user_id', $user->id)->get();
        $my_friends = DB::table('user_friends')
            ->join('user_companies', 'user_companies.user_id', '=', 'user_friends.friend_id')
            ->select('user_companies.*', DB::raw('count(user_companies.company_id) as friends'))
            ->where('user_friends.user_id',$user->id)
            ->groupBy('user_companies.company_id')
            ->get();
        //$my_friends = $my_friends;
        foreach ($my_friends as $key => $value) {

          $friends[$value->company_id] = $value->friends;
        }
        foreach ($my_accounts as $key => $value) {

            $accounts[$value->company_id] = $value->bonus_account;
        }

        foreach ($tradepoints as $key => $value) {

            $pointCoord = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($userCoord)->setTo($pointCoord);

            if (isset($friends[$value->company_id])){
                $friends_cnt = $friends[$value->company_id];
                $friendspoints[] = array(
                    'name' => $value->name,
                    'company' => $value->company->name,
                    'balance' => $balance,
                    'friends' => $friends_cnt,
                    'check_in' => $check_in,
                    'distance' =>  $distance->flat()
                );
                $friends_distance_row[] = $distance->flat();
                $friends_cnt_row[] = $friends_cnt;
            }
            else{
                $friends_cnt = 0;
            }
            if (isset($accounts[$value->company_id])){
                $balance = $accounts[$value->company_id];
                $check_in = 1;
                $mypoints[] = array(
                    'name' => $value->name,
                    'company' => $value->company->name,
                    'balance' => $balance,
                    'friends' => $friends_cnt,
                    'check_in' => $check_in,
                    'distance' =>  $distance->flat()
                );
                $my_distance_row[] = $distance->flat();
            }
            else{
                $balance = null;
                $check_in = 0;
            }

            $allpoints[] = array(
                'name' => $value->name,
                'company' => $value->company->name,
                'balance' => $balance,
                'friends' => $friends_cnt,
                'check_in' => $check_in,
                'distance' => $distance->flat()
            );

            $all_distance_row[$key] = $distance->flat();

        }

       //dump($friends_row);

        //Sort by minimal distance to user
        array_multisort($all_distance_row, SORT_ASC, $allpoints);

        array_multisort($my_distance_row, SORT_ASC, $mypoints);

        array_multisort($friends_cnt_row, SORT_DESC, $friends_distance_row, SORT_ASC, $friendspoints);

        $points = array('all' => $allpoints, 'my' => $mypoints, 'friends' => $friendspoints);

        //dd($points);

        return response()->json(['error_code' => 200, 'points' => $points]);


        //$friends = TradePoints::with('balance')->get();
        foreach ($tradepoints as $key => $value) {
            $balance = $value->balance()->where('user_id', $user->id)->first();
            if ($balance['bonus_account']) $check_in = 1;
            else $check_in = 0;
            //$friends =::with('articles')->get();
            $friends = $value->balance()->join('user_friends','user_companies.user_id', '=', 'user_friends.friend_id')->where('user_friends.user_id', $user->id)->count();
            $points[] = array(
                            'name' => $value->name,
                            'company' => $value->company->name,
                            'balance' => $balance['bonus_account'],
                            'friends' => $friends,
                            'check_in' => $check_in
                      );
        }

       // dd($points);

        $sub = DB::table('user_companies')->select('count(company_id)');

        $tradepoints = DB::table('tradepoints')
            ->join('user_companies', 'user_companies.company_id', '=', 'tradepoints.company_id')
            ->join('companies', 'tradepoints.company_id', '=', 'companies.id')
            //->join('user_friends', 'user_companies.user_id', '=', 'user_friends.friend_id')
            ->select('tradepoints.id','tradepoints.name', 'companies.name as company',
                DB::raw('(select count(user_companies.company_id) from user_companies 
                            inner join tradepoints on user_companies.company_id = tradepoints.company_id
                            inner join user_friends on user_companies.user_id = user_friends.friend_id
                            where user_friends.user_id = '.$user->id.'
                            group by (user_companies.company_id))
                            as friends'),
                DB::raw('(select max(user_companies.bonus_account) from user_companies 
                            inner join tradepoints on user_companies.company_id = tradepoints.company_id
                            where user_companies.user_id = '.$user->id.')
                            as balance'))
            //->groupBy('user_companies.company_id')
            //->where('user_friends.user_id', $user->id)
            // ->union($allpoints)
            ->get();

        //dd($tradepoints);

        // Get all tradepoints
        $all = array();
        $distance_row = array();
       $allpoints  = DB::table('tradepoints')
            ->join('companies', 'tradepoints.company_id', '=', 'companies.id')
            ->select('tradepoints.*', 'companies.name as company')
            ->get();

        foreach ($allpoints as $key => $value){
            $pointCoord = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($userCoord)->setTo($pointCoord);
            $all[] = array(
                'name' => $value->name,
                'company' => $value->company,
                'distance' => $distance->flat(),
                'balance' => 0,
                'friends' => 0,
                'address' => $value->address,
                'city' => $value->city
            );
            $distance_row[] = $distance->flat();
        }

        //Sort by minimal distance to user
        array_multisort($distance_row, SORT_DESC, $all);

        // Get tradepoints with user balance account
        $my = array();
        $distance_row = array();
        $my_accounts = UserCompanies::where('user_id', $user->id)->get();

        dump($my_accounts);
/*        $mypoints = DB::table('user_companies')
            ->join('user_companies', 'user_companies.company_id', '=', 'tradepoints.company_id')
            ->join('companies', 'tradepoints.company_id', '=', 'companies.id')
            ->select('tradepoints.*', 'companies.name as company', 'user_companies.bonus_account as balance')
            ->where('user_companies.user_id', $user->id)
            ->groupBy('tradepoints.company_id')
            //->selectRaw('items.*, COUNT(IF(approved = 0, 1, NULL)) AS unapproved_participants');
            ->get();

        dd($mypoints);*/
        $my_friends = DB::table('user_friends')
                        ->join('user_companies', 'user_companies.user_id', '=', 'user_friends.friend_id')
                        ->where('user_friends.user_id',$user->id)
                        ->get();
        dd($my_friends);
/*        foreach ($mypoints as $key => $value){
            $pointCoord = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($userCoord)->setTo($pointCoord);
            $my[] = array(
                'name' => $value->name,
                'company' => $value->company,
                'distance' => $distance->flat(),
                'balance' => $value->balance,
                'friends' => null,
                'address' => $value->address,
                'city' => $value->city
            );
            $distance_row[] = $distance->flat();
        }*/

        //Sort by minimal distance to user
        array_multisort($distance_row, SORT_DESC, $my);

        // Get tradepoints with user friends balance account

        $friends = array();
        $distance_row = array();
        $friends_row = array();

        $friendspoints = DB::table('tradepoints')
            ->join('companies', 'tradepoints.company_id', '=', 'companies.id')
            ->leftJoin('user_companies', 'tradepoints.company_id', '=', 'user_companies.company_id')
            //->join('user_friends', 'user_companies.user_id', '=', 'user_friends.friend_id')
            ->select('tradepoints.*', DB::raw('(select user_companies.bonus_account from user_companies where user_companies.user_id = '.$user->id.') as account'), 'companies.name as company')
            //->groupBy('user_companies.company_id')
            //->where('user_friends.user_id', $user->id)
           // ->union($allpoints)
            //->select('tradepoints.name', 'companies.name as company')
            ->get();
        //dd($friendspoints);
        $queue = $friendspoints->union($allpoints)->union($mypoints);
        //dd($queue);
        foreach ($queue as $key => $value){
            $pointCoord = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($userCoord)->setTo($pointCoord);
            $friends[$key] = array(
                'name' => $value->name,
                'company' => $value->company,
                'distance' => $distance->flat(),
                'address' => $value->address,
                'city' => $value->city
            );
            if (isset($value->friends)){
                $friends[$key]['friends'] = $value->friends;
            }
            if (isset($value->balance)){
                $friends[$key]['balance'] = $value->balance;
            }
                //
            $distance_row[] = $distance->flat();
            $friends_row[] = $value->bonus;

        }
       // dd($all);
/*        foreach ($all as $key => $value){
            if (isset($friendspoints[$value->id]['friends']))
                $all[$value->id]['friends']  = $friends[$value->id]['friends'];
            if (isset($mypoints[$value->id]['friends']))
                $all[$value->id]['balance']  = $my[$value->id]['balance'];
        }*/


        //Sort by maximum friends count and minimal distance to user
        array_multisort($friends_row, SORT_DESC, $distance_row, SORT_DESC, $friends);

        $points = array('all' => $all, 'my' => $my, 'friends' => $friends);

        return $points;
    }

    public function allcompanies(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (!$user) return response()->json(['error_code' => 401]);

        $tradepoints = TradePoints::all();

        $userCoord = Geotools::coordinate([$user->lat, $user->lng]);
        $points = array();
        $distance_row = array();
        foreach ($tradepoints as $key => $value){
            $pointCoord = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($userCoord)->setTo($pointCoord);
            $points[] = array(
                              'distance' => $distance->flat(),
                              'name' => $value->name,
                              'connected' => $value->connected,
                              'bonus' => $value->bonus,
                             );
            $distance_row[] = $distance->flat();
        }

        array_multisort($distance_row, SORT_DESC, $points);

       return response()->json(['error_code' => 200, 'points' => $points]);

    }

    public function mycompanies(Request $request)
    {

        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (!$user) return response()->json(['error_code' => 401]);

        $tradepoints = DB::table('tradepoints')
            ->join('user_companies', 'user_companies.company_id', '=', 'tradepoints.id')
            ->join('companies', 'tradepoints.company_id', '=', 'companies.id')
            ->select('tradepoints.*', 'companies.name as company', 'user_companies.bonus_account')
            ->where('user_companies.user_id', $user->id)
            ->get();

       // dd($tradepoints);
        $userCoord = Geotools::coordinate([$user->lat, $user->lng]);
        $points = array();
        $distance_row = array();

        foreach ($tradepoints as $key => $value){
            $pointCoord = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($userCoord)->setTo($pointCoord);
            $points[] = array(
                'distance' => $distance->flat(),
                'name' => $value->name,
                'connected' => $value->connected,
                'bonus' => $value->bonus,
            );
            $distance_row[] = $distance->flat();
        }

        array_multisort($distance_row, SORT_DESC, $points);

        return response()->json(['error_code' => 200, 'points' => $points]);
    }

    public function friendscompanies(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (!$user) return response()->json(['error_code' => 401]);

        $tradepoints = DB::table('tradepoints')
            ->join('user_companies', 'user_companies.company_id', '=', 'tradepoints.id')
            ->join('companies', 'tradepoints.company_id', '=', 'companies.id')
            ->join('user_friends', 'user_companies.user_id', '=', 'user_friends.friend_id')
            ->select('tradepoints.*','user_companies.bonus_account', DB::raw('count(user_companies.company_id) as friends'))
            ->groupBy('user_companies.company_id')
            ->where('user_friends.user_id', $user->id)
            ->get();

        $userCoord = Geotools::coordinate([$user->lat, $user->lng]);
        $distance_row = array();
        $friends_row = array();
        $points = array();

        foreach ($tradepoints as $key => $value){
            $pointCoord = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($userCoord)->setTo($pointCoord);
            $points[] = array(
                'distance' => $distance->flat(),
                'name' => $value->name,
                'connected' => $value->connected,
                'bonus' => $value->bonus,
                'friends' => $value->friends,
            );
            $distance_row[] = $distance->flat();
            $friends_row[] = $value->bonus;
        }
        array_multisort($friends_row, SORT_DESC, $distance_row, SORT_DESC, $points);
        return response()->json(['error_code' => 200, 'points' => $points]);

    }
}