<?php

namespace App\Http\Controllers\Api;

use App\AppUser;
use App\User;
use App\UserContacts;
use App\SmsCode;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\CompaniesController;
use Illuminate\Http\Request;
use SoapClient;
use Swagger\Annotations as SWG;

/**
 * @SWG\Swagger(
 *     basePath="",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Discount API"
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 **/
class AuthController extends Controller
{

    /** @SWG\Get(
     *     path="/api/checkmobile/{phone}",
     *     description="Check user phone number before registration or login.",
     *     operationId="api.checkmobile",
     *     produces={"application/json"},
     *     tags={"authentication"},
     *     @SWG\Response(
     *         response=200,
     *         description="Phone number already registered"
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Phone number didn't registered. The sms code was send to this number",
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="An error occurred while sending SMS"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="path",
     *         description="User phone number",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function checkmobile(Request $request)
    {

        $user = AppUser::where('phone', $request->phone)->get();

        if (count($user)) {
            return response()->json(['error_code' => 200]);
        } else {

            $usercode = SmsCode::where('phone', $request->phone)->first();

            $otp = rand(1000, 9999);

            if ($usercode) {

                $usercode->update(['code' => $otp]);
            } else {
                $code = new SmsCode;
                $code->phone = $request->phone;
                $code->code = $otp;
                $code->save();
            }

            return response()->json(['error_code' => 201, 'code' => $otp]);
        }

        return response()->json(['error_code' => 500]);
    }

    /** @SWG\Get(
     *     path="/api/checksms/{phone}/{code}",
     *     description="Check sms code to confirm phone number.",
     *     operationId="api.checksms",
     *     produces={"application/json"},
     *     tags={"authentication"},
     *     @SWG\Response(
     *         response=200,
     *         description="Sms code is valid"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Invalid sms code",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="path",
     *         description="User phone number",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="code",
     *         in="path",
     *         description="Sms code",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function checksms(Request $request)
    {

        $usercode = SmsCode::where('phone', $request->phone)->first();

        if ($usercode->code == $request->code) {
            return response()->json(['error_code' => 200]);
        } else {
            return response()->json(['error_code' => 401]);
        }

    }

    /** @SWG\Post(
     *     path="/api/create",
     *     description="Create new user.",
     *     operationId="api.create",
     *     produces={"application/x-www-form-urlencoded"},
     *     consumes = {"application/x-www-form-urlencoded"},
     *     tags={"authentication"},
     *     @SWG\Response(
     *         response=409,
     *         description="Phone number is already used"
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="New user was created sucessfull",
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Internal server error",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="User phone number",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User password",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="lat",
     *         in="formData",
     *         description="Latitude",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lng",
     *         in="formData",
     *         description="Longitude",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function create(Request $request)
    {

        $user = AppUser::where('phone', $request->phone)->first();

        if (count($user)) {
            return response()->json(['error_code' => 409]);
        }

        $user = new AppUser;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->save();

        if ($user->id) {
            $token = Auth::guard('api')->attempt(['phone' => $request->phone, 'password' => $request->password]);
            $user = Auth::guard('api')->user();

            $countries = DB::table('apps_countries')->pluck('country_code');

            if (isset($request->lat) && isset($request->lng)) {
                $user->lat = $request->lat;
                $user->lng = $request->lng;
                $user->save();
            }

            if (isset($request->contacts)) {
                $contacts = json_decode($request->contacts, true);
            }

            if (!empty($contacts)) {
                $friends = array();
                foreach ($contacts as $item) {

                    $contact = new UserContacts;
                    $contact->user_id = $user->id;
                    $contact->phone = $item['phone'];
                    $contact->name = $item['name'];
                  /*  $contact->email = $item['email'];*/
                    $contact->save();
                    $appuser = AppUser::where('phone', $item['phone'])
                        ->orWhere('phone', 'like', '%' . $item['phone'] . '%')->first();
                    if (count($appuser)) {
                        $friends[] = array(
                            'user_id' => $user->id,
                            'friend_id' => $appuser->id,
                            'contact_id' => $contact->id,
                        );
                    }
                }
                DB::table('user_friends')->insert($friends);
            }

           return response()->json(['error_code' => 201, 'token' => $token, 'user_id' => $user->id, 'countries' => $countries]);
        }

        return response()->json(['error_code' => 500]);
    }

    /** @SWG\Post(
     *     path="/api/signin",
     *     description="User login.",
     *     operationId="api.signin",
     *     produces={"application/x-www-form-urlencoded"},
     *     tags={"authentication"},
     *     @SWG\Response(
     *         response=200,
     *         description="User was signed in successfully"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Invalid password",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Phone number not found",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="User phone number",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User password",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lat",
     *         in="formData",
     *         description="Latitude",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lng",
     *         in="formData",
     *         description="Longitude",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function signin(Request $request)
    {

        $user = AppUser::where('phone', $request->phone)->first();

        if (count($user)) {

            if (\Hash::check($request->password, $user->password)) {
                $token = Auth::guard('api')->attempt(['phone' => $request->phone, 'password' => $request->password]);
                $appuser = Auth::guard('api')->user();

                if (isset($request->lat) && isset($request->lng)) {
                    $user->lat = $request->lat;
                    $user->lng = $request->lng;
                    $user->save();
                }

                $points = CompaniesController::getcompanies($user);

                return response()->json(['error_code' => 200, 'token' => $token, 'user_id' => $appuser->id, 'points' => $points]);
            } else {
                return response()->json(['error_code' => 401]);

            }
        } else {
            return response()->json(['error_code' => 404]);
        }

    }

    /** @SWG\Get(
     *     path="/api/logout/{token}",
     *     description="User logout",
     *     operationId="api.logout",
     *     produces={"application/json"},
     *     tags={"authentication"},
     *     @SWG\Response(
     *         response=200,
     *         description="User logout"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Invalid token"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="path",
     *         description="User token",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function logout(Request $request)
    {
        try {
            JWTAuth::setToken($request->token)->invalidate();
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        return response()->json(['error_code' => 200]);
    }

    /** @SWG\Get(
     *     path="/api/forgot/{phone}",
     *     description="User forgot password.",
     *     operationId="api.forgot",
     *     produces={"application/json"},
     *     tags={"authentication"},
     *     @SWG\Response(
     *         response=200,
     *         description="Sms code was send to user phone number"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="An error occurred while sending SMS",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Phone number not found",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="path",
     *         description="User phone number",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function forgot(Request $request)
    {

        $user = AppUser::where('phone', $request->phone)->first();

        if (count($user)) {

            $usercode = SmsCode::where('phone', $request->phone)->first();

            $otp = rand(1000, 9999);

            if ($usercode) {

                $usercode->update(['code' => $otp]);
            } else {

                $code = new SmsCode;
                $code->phone = $request->phone;
                $code->code = $otp;
                $code->save();
            }

            return response()->json(['error_code' => 200, 'code' => $otp]);
        } else {
            return response()->json(['error_code' => 404]);
        }

        return response()->json(['error_code' => 500]);
    }

    /** @SWG\Post(
     *     path="/api/reset",
     *     description="Reset user password.",
     *     operationId="api.reset",
     *     produces={"application/x-www-form-urlencoded"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     tags={"authentication"},
     *     @SWG\Response(
     *         response=200,
     *         description="User password was updated successfully"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Phone number not found",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="User phone number",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="new_password",
     *         in="formData",
     *         description="New user password",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function reset(Request $request)
    {

        $user = AppUser::where('phone', $request->phone)->first();

        if (count($user)) {

            $user->update(['password' => bcrypt($request->new_password)]);

            return response()->json(['error_code' => 200, 'password' => $request->new_password]);
        } else {
            return response()->json(['error_code' => 404]);
        }

    }

    /** @SWG\Post(
     *     path="/api/changepassword",
     *     description="User password change.",
     *     operationId="api.changepassword",
     *     produces={"application/x-www-form-urlencoded"},
     *     consumes = {"application/x-www-form-urlencoded"},
     *     tags={"profile"},
     *     @SWG\Response(
     *         response=200,
     *         description="User password was changed successfull"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Invalid token"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Invalid old password",
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="formData",
     *         description="User token",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="old_password",
     *         in="formData",
     *         description="User old password",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="new_password",
     *         in="formData",
     *         description="User new password",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function changepassword(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (\Hash::check($request->old_password, $user->password)) {

            $user->update(['password' => bcrypt($request->new_password)]);

            return response()->json(['error_code' => 200]);
        }
        else {
            return response()->json(['error_code' => 403]);
        }
    }

    /** @SWG\Post(
     *     path="/api/editprofile",
     *     description="Edit user profile.",
     *     operationId="api.editprofile",
     *     produces={"application/x-www-form-urlencoded"},
     *     consumes = {"application/x-www-form-urlencoded"},
     *     tags={"profile"},
     *     @SWG\Response(
     *         response=200,
     *         description="User profile was changed successfully"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Invalid token"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="formData",
     *         description="User token",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="User name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="surname",
     *         in="formData",
     *         description="User surname",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="birthday",
     *         in="formData",
     *         description="User birthday",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="gender",
     *         in="formData",
     *         description="User gender",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="region",
     *         in="formData",
     *         description="User region",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User email",
     *         required=false,
     *         type="string"
     *     )
     * )
     **/

    public function editprofile(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if (count($user)) {

           // $user->update($request->all());

            //return response()->json(['error_code' => 200, 'user' => $user]);

            if (isset($request->name)){
                $user->name = $request->name;
            }
            if (isset($request->surname)){
                $user->surname = $request->surname;
            }
            if (isset($request->birthday)) {
                $user->birthday = $request->birthday;
            }
            if (isset($request->gender)) {
                $user->gender = $request->gender;
            }
            if (isset($request->region)) {
                $user->region = $request->region;
            }
            if (isset($request->email)) {
                $user->email = $request->email;
            }

            $user->save();

            return response()->json(['error_code' => 200, 'user' => $user]);
        } else {
            return response()->json(['error_code' => 401]);
        }

    }

    /** @SWG\Get(
     *     path="/api/profile/{token}",
     *     description="User profile dashboard.",
     *     operationId="api.profile",
     *     produces={"application/json"},
     *     tags={"profile"},
     *     @SWG\Response(
     *         response=200,
     *         description="User profile is available for display"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Invalid token"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="path",
     *         description="User token",
     *         required=true,
     *         type="string"
     *     )
     * )
     **/

    public function profile(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        return response()->json(['error_code' => 200, 'user' => $user]);
    }

    /** @SWG\Post(
     *     path="/api/uploadphoto",
     *     description="User upload photo.",
     *     operationId="api.uploadphoto",
     *     produces={"application/x-www-form-urlencoded"},
     *     tags={"profile"},
     *     @SWG\Response(
     *         response=200,
     *         description="User photo was updated succesfully"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Invalid token"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="formData",
     *         description="User token",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="photo",
     *         in="formData",
     *         description="Photo file",
     *         required=true,
     *         type="file"
     *     )
     * )
     **/

    public function uploadphoto(Request $request)
    {

        try {
            $user = JWTAuth::toUser($request->token);
        }
        catch (Exception $exception) {
            return response()->json(['error_code' => 401]);
        }

        if ($request->file('photo') != null) {


            $imageName = str_random(30) . '.' .
                $request->file('photo')->getClientOriginalExtension();

            $request->file('photo')->move(
                base_path() . '/public/images/users/', $imageName
            );

            $path = '/images/users/' . $imageName;
            $user->photo = $path;
            $user->save();

            return response()->json(['error_code' => 200, 'path' => $path]);

        } else {
            return response()->json(['error_code' => 401]);
        }
    }


    public function sms(Request $request)
    {

        try {

            // Подключаемся к серверу   
            $client = new \SoapClient('http://turbosms.in.ua/api/wsdl.html');

            // Можно просмотреть список доступных методов сервера   
            print_r($client->__getFunctions());

            // Данные авторизации   
            $auth = [
                'login' => 'discount',
                'password' => 'admin123'
            ];

            // Авторизируемся на сервере   
            $result = $client->Auth($auth);

            // Результат авторизации   
            echo $result->AuthResult . PHP_EOL;

            // Получаем количество доступных кредитов   
            $result = $client->GetCreditBalance();
            echo $result->GetCreditBalanceResult . PHP_EOL;

            // Текст сообщения ОБЯЗАТЕЛЬНО отправлять в кодировке UTF-8   
            $text = iconv('windows-1251', 'utf-8', 'Тестовое сообщение');

            // Отправляем сообщение на один номер.   
            // Подпись отправителя может содержать английские буквы и цифры. Максимальная длина - 11 символов.   
            // Номер указывается в полном формате, включая плюс и код страны   
            $sms = [
                'sender' => 'Discount',
                'destination' => '+380506874594',
                'text' => $text
            ];
            $result = $client->SendSMS($sms);

            // Отправляем сообщение на несколько номеров.   
            // Номера разделены запятыми без пробелов.   
            /*            $sms = [
                            'sender' => 'Rassilka',
                            'destination' => '+380XXXXXXXX1,+380XXXXXXXX2,+380XXXXXXXX3',
                            'text' => $text
                        ];
                        $result = $client->SendSMS($sms);
            */
            print_r($result);
            print_r($result->SendSMSResult);
            print_r($result->SendSMSResult->ResultArray);

            $id = $result->SendSMSResult->ResultArray[0];
            // Выводим результат отправки.   
            /*            echo $result->SendSMSResult->ResultArray[0] . PHP_EOL;

                        // ID первого сообщения
                        echo $result->SendSMSResult->ResultArray[1] . PHP_EOL;

                        // ID второго сообщения
                        echo $result->SendSMSResult->ResultArray[2] . PHP_EOL;   */

            // Отправляем сообщение с WAPPush ссылкой   
            // Ссылка должна включать http://   
            /*            $sms = [
                            'sender' => 'Rassilka',
                            'destination' => '+380XXXXXXXXX',
                            'text' => $text,
                            'wappush' => 'http://super-site.com'
                        ];
                        $result = $client->SendSMS($sms);  */

            // Запрашиваем статус конкретного сообщения по ID   
            $sms = ['MessageId' => $id];
            $status = $client->GetMessageStatus($sms);
            echo $status->GetMessageStatusResult . PHP_EOL;

        } catch (Exception $e) {
            echo 'Ошибка: ' . $e->getMessage() . PHP_EOL;
        }
    }

}