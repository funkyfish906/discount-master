@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
       

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="info-block">
            Название торговой точки и логотип
            
            <h1><img class="logo" src="/uploads/thumb/{{$tradepoints->logo}}" />  {{$tradepoints->name}}</h1>
        </div>
        <div class="info-block">
            Краткое описание до 256 символов
            {{$tradepoints->description}}
        </div>
    </div>
    <div class="col-md-6">
        <div class="info-block">
            Подключен к DISCOUNTU
        </div>
        <div class="info-block">
            Бонус {{$tradepoints->bonus}}
        </div>
        <div class="info-block">
            Фотогалерея
            @foreach ($gallery as $photo)
            <img class="logo" src="/uploads/thumb/{{($photo->photo)}}" />
            @endforeach
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="info-block">
            Адресс и контактные данные: </br>
            <div class="field">
                <span class="label-text">Страна</span>
                <span class="value-text">{{$tradepoints->country}}</span>
            </div>
            <div class="field">
                <span class="label-text">Город</span>
                <span class="value-text">{{$tradepoints->city}}</span>
            </div>
            <div class="field">
                <span class="label-text">Улица</span>
                <span class="value-text">{{$tradepoints->address}}</span>
            </div>
            <div class="field">
                <span class="label-text">Примечание</span>
                <span class="value-text">{{$tradepoints->footnote}}</span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="info-block">
            <div class="field">
                <span class="label-text">Телефон</span>
                <span class="value-text">{{$tradepoints->phone}}</span>
            </div>
            <div class="field">
                <span class="label-text">Електронная почта</span>
                <span class="value-text">{{$tradepoints->email}}</span>
            </div>
            <div class="field">
                <span class="label-text">Сайт</span>
                <span class="value-text">{{$tradepoints->website}}</span>
            </div>
        </div>
    </div>
</div>
@if (!empty($schedule))
<div class="row">
    <div class="col-md-6"> 
        <div class="info-block">

            График работы торговой точки: <br />
	            @for($i = 0; $i < 7; $i++)
                    <div class="field">
                        <span class="label-text">{{$schedule['days'][$i]}}</span>
                        <span class="value-text">{{$schedule['time'][$i]}}</span>
                    </div>
	            @endfor

        </div>
    </div>
    <div class="col-md-6">
        <div class="info-block">
	            @foreach($schedule['break'] as $val)
		            @if ($val!= null)
			            <div class="field">
			                <span class="label-text">Перерыв</span>
			                <span class="value-text">{{$val}}</span>
			            </div>
		            @endif
	            @endforeach
        </div>
    </div>
</div>
@endif

{!! Form::close() !!}

@endsection