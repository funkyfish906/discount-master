<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    /* AuthController */

    Route::post('/checkmobile', 'Api\AuthController@checkmobile')->name('checkmobile');
    Route::get('/checkmobile/{phone}', 'Api\AuthController@checkmobile')->name('checkmobile');
	Route::post('/checksms', 'Api\AuthController@checksms')->name('checksms');
    Route::get('/checksms/{phone}/{code}', 'Api\AuthController@checksms')->name('checksms');
    Route::post('/create', 'Api\AuthController@create')->name('create');
    Route::get('/create/{phone}/{password}', 'Api\AuthController@create')->name('create');
    Route::post('/signin', 'Api\AuthController@signin')->name('signin');
    Route::get('/signin/{phone}/{password}', 'Api\AuthController@signin')->name('signin');
    Route::post('/forgot', 'Api\AuthController@forgot')->name('forgot');
    Route::get('/forgot/{phone}', 'Api\AuthController@forgot')->name('forgot');
    Route::post('/changepassword', 'Api\AuthController@changepassword')->name('changepassword');
    Route::get('/changepassword/{phone}/{old_password}/{new_password}/', 'Api\AuthController@changepassword')->name('changepassword');
    Route::post('/profile', 'Api\AuthController@profile')->name('profile');
    Route::get('/profile/{token}', 'Api\AuthController@profile')->name('profile');
    Route::post('/editprofile', 'Api\AuthController@editprofile')->name('editprofile');
    Route::get('/editprofile/{phone}/{name}', 'Api\AuthController@editprofile')->name('editprofile');
	Route::get('/sms', 'Api\AuthController@sms')->name('sms');
    Route::post('/reset', 'Api\AuthController@reset');
    Route::get('/logout/{token}', 'Api\AuthController@logout');
    Route::post('/uploadphoto', 'Api\AuthController@uploadphoto');

    /* CostomersController */

    Route::get('/customer/signin/{username}/{password}', 'Api\CustomersController@signin');
    Route::middleware('jwt.auth')->get('/customer/scan/{phone}/{invoice}/{token}', 'Api\CustomersController@scan');
    Route::middleware('jwt.auth')->post('/customer/scan', 'Api\CustomersController@scan');
    Route::post('/customer/confirm', 'Api\CustomersController@confirm');
    Route::get('/customer/logout/{token}', 'Api\CustomersController@logout');
    Route::post('/customer/addtransaction/', 'Api\CustomersController@addtransaction');
    Route::get('/customer/list/{token}', 'Api\CustomersController@list');

    /* CompaniesController */

    Route::post('/usercontacts', 'Api\CompaniesController@usercontacts');
    Route::get('/company/info/{id}/{token}', 'Api\CompaniesController@companyinfo');
    Route::get('/tradepoint/info/{id}/{token}', 'Api\CompaniesController@tradepointinfo');
    Route::get('/companies/all/{token}', 'Api\CompaniesController@allcompanies');
    Route::get('/companies/my/{token}', 'Api\CompaniesController@mycompanies');
    Route::get('/companies/friends/{token}', 'Api\CompaniesController@friendscompanies');

    Route::get('/distance', 'Api\CustomersController@distance');
